#include <xc.h>
#include "IO.h"

#define START_HIGH_MIN 124
#define START_HIGH_MAX 151
#define START_LOW_MIN 50
#define START_LOW_MAX 64


uint16_t high_cnt=0;
uint16_t low_cnt=0;

uint32_t timetick = 0;
uint32_t relay_cnt;

void interrupt kes(void){

    if(PIR0bits.TMR0IF && PIE0bits.TMR0IE){

        timetick++;
        PIR0bits.TMR0IF=0;
        //LED1 ^=1;
        if(DATA){
            high_cnt++;
        }else{
            low_cnt++;
        }


    }

    if(IOCAFbits.IOCAF0){   //rising edge detected

        if(DATA)
        {
            if(look_for_start)
            {
                if(high_cnt >= START_HIGH_MIN && low_cnt >= START_LOW_MIN && high_cnt <= START_HIGH_MAX && low_cnt <= START_LOW_MAX){//here comes the data again,falling on my head like a memory
                    nth_bit=0;
                    Send_flag=0;
                    look_for_start = 0;
                    LED1 ^= 1;
                    LED1 ^= 1;
                    LED1 ^= 1;
                    LED1 ^= 1;
                    LED1 ^= 1;
                }
            }else if(high_cnt + low_cnt < 75){
                    if(high_cnt >= 27 ){
                        Send_flag=0;
                        Get_data(1);
                        LED1 ^= 1;
                        LED1=1;
                    }else if(low_cnt >= 27){
                        Send_flag=0;
                        Get_data(0);
                        LED1 ^= 1;
                        LED1=0;
                    }
                }else{
                    for(int i=0;i<16;i++)
                        datas[i] = (uint8_t) 0;
                    look_for_start = 1;
                }



          high_cnt=0;
          low_cnt=0;
          IOCAFbits.IOCAF0=0;
        }
    }

}
